﻿namespace ShapeCreator.Tests.TestHelpers
{
    public class TestDataPaths
    {
        internal const string OnePixelSquareBlankCanvasFilePath = "C_1_1_BlankCanvas.txt";
        internal const string OnePixelFilledCanvasFilePath = "C_1_1_L1_1_1_1.txt";

        internal const string BlankCanvas20X4FilePath = "C_20_4_BlankCanvas.txt";
        internal const string Canvas20X4With1LineFilePath = "C_20_4_L1_2_6_2.txt";
        internal const string Canvas20X4With2LinesFilePath = "C_20_4_L1_2_6_2_L6_3_6_4.txt";

        internal const string Canvas20X4With1RectangleFilePath = "C_20_4_R_14_1_18_3.txt";
        internal const string Canvas20X4WithFullBucketFilledFilePath = "C_20_4_B_10_3_o.txt";
        internal const string Canvas20X4With1RectangleForBucketFillingFilePath = "C_20_4_R_14_1_18_3_B_10_3_o.txt";

        internal const string CommandWith20X4CanvasInputFilePath = "CommandWith20X4CanvasInput.txt";
        internal const string CommandWith20X4CanvasOutputCanvasFilePath = "CommandWith20X4CanvasOutputCanvas.txt";

        internal const string CommandWith20X4Canvas1LineInputFilePath = "CommandWith20X4Canvas1LineInput.txt";
        internal const string CommandWith20X4Canvas1LineOutputCanvasFilePath = "CommandWith20X4Canvas1LineOutputCanvas.txt";

        internal const string CommandWith20X4Canvas2Lines1Rectangle1BucketFillInputFilePath = "CommandWith20X4Canvas2Lines1Rectangle1BucketFillInput.txt";
        internal const string CommandWith20X4Canvas2Lines1Rectangle1BucketFillOutputCanvasFilePath = "CommandWith20X4Canvas2Lines1Rectangle1BucketFillOutputCanvas.txt";

        internal const string DefaultTestDataFolderPath = "TestData";

    }
}
