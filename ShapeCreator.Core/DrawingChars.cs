﻿namespace ShapeCreator.Core
{
    public class DrawingChars
    {
        public const char DefaultFillChar = 'x';
        public const char BlankSpaceChar = ' ';
        public const char HorizontalCanvasBorderChar = '-';
        public const char VirticalCanvasBorderChar = '|';
    }
}