﻿namespace ShapeCreator.Core.Exceptions
{
    public class DrawingException : CoreException
    {
        public DrawingException(string message = null):base(message)
        {
           
        }
    }
}