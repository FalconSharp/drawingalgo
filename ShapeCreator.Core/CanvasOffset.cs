﻿namespace ShapeCreator.Core
{
    public class CanvasOffset
    {
        public const int RowBorderOffset = 2;
        public const int ColumnBorderOffset = 2;
    }
}