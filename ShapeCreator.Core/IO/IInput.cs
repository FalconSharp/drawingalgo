﻿namespace ShapeCreator.Core.IO
{
    public interface IInput
    {
        string ReadLine();
    }
}