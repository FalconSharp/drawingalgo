﻿namespace ShapeCreator.Core.Command.Commands
{
    public interface ICommandCore
    {
        string CommandName { get; }
    }
}