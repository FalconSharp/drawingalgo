﻿namespace ShapeCreator.Core.Command.Infrastructure
{
    public enum CommandType { Basic, CreateCanvas, DrawInCanvas}
}